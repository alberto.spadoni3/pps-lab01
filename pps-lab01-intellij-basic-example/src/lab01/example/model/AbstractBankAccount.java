package lab01.example.model;

public abstract class AbstractBankAccount implements BankAccount {

    protected double balance;
    protected final AccountHolder holder;

    protected AbstractBankAccount(final AccountHolder holder, final double balance) {
        this.balance = balance;
        this.holder = holder;
    }

    @Override
    public AccountHolder getHolder() {
        return this.holder;
    }

    @Override
    public double getBalance() {
        return this.balance;
    }
}
