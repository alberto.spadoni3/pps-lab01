package lab01.example.model;

import lab01.example.utils.TransactionVerifications;

public class SimpleBankAccountWithAtm extends AbstractBankAccount {

    private static final int ATM_FEE = 1;

    public SimpleBankAccountWithAtm(AccountHolder holder, double balance) {
        super(holder, balance);
    }

    @Override
    public void deposit(int usrID, double amount) {
        if (TransactionVerifications.checkUser(holder, usrID))
            this.balance += applyFee(TransactionVerifications.DEPOSIT, amount);
    }

    @Override
    public void withdraw(int usrID, double amount) {
        if (TransactionVerifications.checkUser(holder, usrID) &&
            TransactionVerifications.isWithdrawAllowed(this.getBalance(), amount))
            this.balance -= applyFee(TransactionVerifications.WITHDRAWAL, amount);
    }

    private double applyFee(final String transactionType, double amount) {
        if (transactionType.equals(TransactionVerifications.DEPOSIT))
            amount -= ATM_FEE;
        else if (transactionType.equals(TransactionVerifications.WITHDRAWAL))
            amount += ATM_FEE;
        return amount;
    }
}
