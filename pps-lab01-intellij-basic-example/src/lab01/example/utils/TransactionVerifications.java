package lab01.example.utils;
import lab01.example.model.AccountHolder;

public class TransactionVerifications {
    public final static String DEPOSIT = "deposit",
                               WITHDRAWAL = "withdraw";

    public static boolean isWithdrawAllowed(final double balance, final double amount){
        return balance >= amount;
    }

    public static boolean checkUser(final AccountHolder holderID, final int id) {
        return holderID.getId() == id;
    }
}
