import lab01.example.model.AccountHolder;
import lab01.example.model.BankAccount;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public abstract class BankAccountTest {
    protected final double INITIAL_BALANCE = 1000,
                           AMOUNT_TO_DEPOSIT = 200,
                           AMOUNT_TO_WITHDRAW = 200;
    protected final int ACCOUNT_HOLDER_ID = 1;
    protected int ATM_FEE = 0;

    protected AccountHolder accountHolder;
    protected BankAccount bankAccount;

    abstract void beforeEach();

    @Test
    protected void testInitialBalance() {
        assertEquals(INITIAL_BALANCE, bankAccount.getBalance());
    }

    @Test
    protected void testDeposit() {
        bankAccount.deposit(accountHolder.getId(), AMOUNT_TO_DEPOSIT);
        assertEquals(afterDeposit(AMOUNT_TO_DEPOSIT, ATM_FEE), bankAccount.getBalance());
    }

    @Test
    protected void testWrongDeposit() {
        bankAccount.deposit(accountHolder.getId(), AMOUNT_TO_DEPOSIT);
        bankAccount.deposit(2, 50);
        assertEquals(afterDeposit(AMOUNT_TO_DEPOSIT, ATM_FEE), bankAccount.getBalance());
    }

    @Test
    protected void testWithdraw() {
        bankAccount.withdraw(accountHolder.getId(), AMOUNT_TO_WITHDRAW);
        assertEquals(afterWithdrawal(AMOUNT_TO_WITHDRAW, ATM_FEE), bankAccount.getBalance());
    }

    @Test
    protected void testWrongWithdraw() {
        bankAccount.withdraw(2, AMOUNT_TO_WITHDRAW);
        assertEquals(INITIAL_BALANCE, bankAccount.getBalance());
    }

    private double afterDeposit(final double amount, final int fee) {
        return INITIAL_BALANCE + amount - fee;
    }

    private double afterWithdrawal(final double amount, final int fee) {
        return INITIAL_BALANCE - amount - fee;
    }
}
