import lab01.tdd.CircularList;
import lab01.tdd.strategy.strategies.SelectStrategy;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class CircularListImpl implements CircularList {

    private enum Direction {
        NEXT,
        PREVIOUS
    }

    private final List<Integer> list = new LinkedList<>();
    private int currentElement = 0;

    @Override
    public void add(int element) {
        this.list.add(element);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return this.list.size() == 0;
    }

    @Override
    public Optional<Integer> next() {
        Optional<Integer> nextElement;
        if (list.isEmpty())
            nextElement = Optional.empty();
        else
            nextElement = this.getElement(Direction.NEXT);
        return nextElement;
    }

    @Override
    public Optional<Integer> previous() {
        Optional<Integer> previousElement;
        if (list.isEmpty())
            previousElement = Optional.empty();
        else
            previousElement = this.getElement(Direction.PREVIOUS);
        return previousElement;
    }

    private Optional<Integer> getElement(Direction direction) {
        int element = 0;
        if (direction == Direction.NEXT) {
            if (this.currentElement == this.list.size())
                this.currentElement = 0;
            element = this.list.get(this.currentElement++);
        }
        else if (direction == Direction.PREVIOUS) {
            if (this.currentElement == 0)
                this.currentElement = this.list.size();
            element = this.list.get(--this.currentElement);
        }
        return Optional.of(element);
    }

    @Override
    public void reset() {
        this.currentElement = 0;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        int currentElement = this.next().get();

        while (!strategy.apply(currentElement))
             currentElement = this.next().get();

        return Optional.of(currentElement);
    }
}
