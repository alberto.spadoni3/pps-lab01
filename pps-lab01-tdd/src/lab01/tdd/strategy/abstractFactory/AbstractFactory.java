package lab01.tdd.strategy.abstractFactory;

import lab01.tdd.strategy.strategies.SelectStrategy;
import lab01.tdd.strategy.abstractFactory.StrategyFactory.StrategyTypes;
import java.util.Optional;

public abstract class AbstractFactory {
    public abstract SelectStrategy of(StrategyTypes type, Optional<Integer> strategyInput);
}
