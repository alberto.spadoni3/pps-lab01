package lab01.tdd.strategy.abstractFactory;

import lab01.tdd.strategy.strategies.*;

import java.util.Optional;

public class StrategyFactory extends AbstractFactory {
    @Override
    public SelectStrategy of(StrategyTypes type, Optional<Integer> strategyInput) {
        return strategyInput.isPresent() ?
                selectStrategy(type, strategyInput) :
                selectStrategy(type, Optional.empty());
    }

    private SelectStrategy selectStrategy(StrategyTypes type, Optional<Integer> strategyInput) {
        SelectStrategy newStrategy;
        switch (type) {
            case MULTIPLE:
                newStrategy = new MultipleStrategy(strategyInput.get());
                break;
            case EQUALS:
                newStrategy = new EqualsStrategy(strategyInput.get());
                break;
            case EVEN:
                newStrategy = new EvenStrategy();
                break;
            case ODD:
                newStrategy = new OddStrategy();
                break;
            default:
                throw new IllegalArgumentException("Strategy not available yet");
        }
        return newStrategy;
    }

    public enum StrategyTypes {
        EQUALS,
        MULTIPLE,
        EVEN,
        ODD
    }
}
