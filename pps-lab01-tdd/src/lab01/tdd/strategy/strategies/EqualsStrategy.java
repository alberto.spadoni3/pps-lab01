package lab01.tdd.strategy.strategies;

public class EqualsStrategy implements SelectStrategy {
    private final int equalElement;

    public EqualsStrategy(int equalElement) {
        this.equalElement = equalElement;
    }

    @Override
    public boolean apply(int element) {
        return element == this.equalElement;
    }
}
