package lab01.tdd.strategy.strategies;

public class MultipleStrategy implements SelectStrategy {
    private final int multiple;

    public MultipleStrategy(int multiple) {
        this.multiple = multiple;
    }

    @Override
    public boolean apply(int element) {
        return element % this.multiple == 0;
    }
}
