import lab01.tdd.*;

import static org.junit.jupiter.api.Assertions.*;

import lab01.tdd.strategy.abstractFactory.AbstractFactory;
import lab01.tdd.strategy.abstractFactory.StrategyFactory;
import lab01.tdd.strategy.abstractFactory.StrategyFactory.StrategyTypes;
import lab01.tdd.strategy.strategies.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {
    private CircularList list = new CircularListImpl();
    private final AbstractFactory factory = new StrategyFactory();

    private final int[] listElements = {12, 3, 88, 7653, 345, 221, 20320};
    private final int FIRST_ELEMENT = this.listElements[0];
    private final int LAST_ELEMENT = this.listElements[this.listElements.length - 1];

    @BeforeEach
    void beforeTest() {
        this.list = new CircularListImpl();
        this.addElements();
    }

    @Test
    void testEmpty() {
        assertFalse(list.isEmpty());
    }

    @Test
    void testListSize() {
        assertEquals(this.listElements.length, this.list.size());
    }

    @Test
    void testAddElement() {
        list.add(this.FIRST_ELEMENT);
        assertEquals(this.listElements.length + 1, this.list.size());
    }

    @Test
    void testNext() {
        assertEquals(Optional.of(this.FIRST_ELEMENT), this.list.next());
    }

    @Test
    void testPrevious() {
        assertEquals(Optional.of(this.LAST_ELEMENT), this.list.previous());
    }

    @Test
    void testCircularMechanism() {
        int ITERATIONS = 50;

        for (int i = 1; i < ITERATIONS; i++) {
            this.list.next();
        }
        assertEquals(Optional.of(this.listElements[ITERATIONS % this.list.size() - 1]), this.list.next());

        this.list.reset();

        for (int i = 1; i < ITERATIONS; i++) {
            this.list.previous();
        }
        assertEquals(Optional.of(this.listElements[this.list.size() - (ITERATIONS % this.list.size())]), this.list.previous());
    }

    @Test
    void testReset() {
        this.list.previous();
        this.list.reset();

        assertEquals(Optional.of(this.FIRST_ELEMENT), this.list.next());
    }

    @Test
    void testOddEvenStrategies() {
        //EVEN STRATEGY
        SelectStrategy strategy = this.factory.of(StrategyTypes.EVEN, Optional.empty());
        assertEquals(Optional.of(this.FIRST_ELEMENT), this.list.next(strategy));
        this.list.next(strategy);
        assertEquals(Optional.of(this.LAST_ELEMENT), this.list.next(strategy));

        // ODD STRATEGY
        strategy = this.factory.of(StrategyFactory.StrategyTypes.ODD, Optional.empty());
        this.list.reset();
        assertEquals(Optional.of(this.listElements[1]), this.list.next(strategy));
        this.list.next(strategy);
        assertEquals(Optional.of(this.listElements[4]), this.list.next(strategy));
    }

    @Test
    void testMultipleStrategy() {
        SelectStrategy multiple = this.factory.of(StrategyTypes.MULTIPLE, Optional.of(5));
        assertEquals(Optional.of(this.listElements[4]), this.list.next(multiple));
        assertEquals(Optional.of(this.LAST_ELEMENT), this.list.next(multiple));
    }

    @Test
    void testEqualsStrategy() {
        SelectStrategy equals = this.factory.of(StrategyTypes.EQUALS, Optional.of(this.LAST_ELEMENT));
        assertEquals(Optional.of(this.LAST_ELEMENT), this.list.next(equals));
    }

    private void addElements() {
        for (int element : this.listElements)
            list.add(element);
    }
}
